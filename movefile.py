import numpy as np
import matplotlib.pyplot as plt
import sys

def move(car1position, car2position, car1speed, override=False):
	"""
	Function to move car 1 while taking into account the relative position of car 2. Car 2 is the car directly in front of car 1, which has already been moved for this tick of the simulation (moving cars from mile 100 descending to mile 0). Moves each car a distance equal to its velocity (d=v*t, t=1). Maximum speed is 1, minimum distance between cars is .1.
	"""
	car1position = float(car1position)
	car2position = float(car2position)
	upperlimit = 1.0
	lowerlimit = 0.25
	if override == True:   
		car1speed = car1speed

	#if the distance between the two cars is less than upperlimit+lowerlimit, set car1 speed to the maximum
        elif car2position-car1position > upperlimit+lowerlimit:
		car1speed = upperlimit
         
	#if current speed moves car 1 past car2, set speed to that which will put car1 (lowerlimit) behind car 2.
	elif  car1position+car1speed > car2position:
		car1speed = car2position-car1position-lowerlimit

	elif car2position-car1position-car1speed < lowerlimit:
		car1speed -= (car2position-car1position-car1speed)


	car1position += car1speed
	return car1speed, car1position
